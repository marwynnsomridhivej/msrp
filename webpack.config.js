const path = require("path");

module.exports = {
    mode: "production",
    target: "electron-renderer",
    entry: {
        app: "./src/js/index.js",
    },
    output: {
        filename: "index.js",
        path: path.resolve(__dirname, "dist/js"),
    },
    optimization: {
        minimize: true,
    },
    module: {
        rules: [
            {
                test: /\.(jsx|js)$/,
                include: path.resolve(__dirname, "src", "js"),
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            presets: [
                                [
                                    "@babel/preset-env",
                                    {
                                        targets: "defaults",
                                    },
                                ],
                                "@babel/preset-react",
                            ],
                        },
                    },
                ],
            },
        ],
    },
};
