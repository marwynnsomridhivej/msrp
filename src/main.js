const {
    app,
    dialog,
    ipcMain,
    screen,
    shell,
    BrowserWindow,
    Menu,
    MenuItem,
    Notification,
    Tray,
    nativeImage,
} = require("electron");

// LOCK SINGLE INSTANCE
if (!app.requestSingleInstanceLock()) {
    app.quit();
}

// RESUME REQUIRES
const fs = require("fs").promises;
const fetch = require("node-fetch");
const path = require("path");
const appConfigPath = path.join(app.getPath("userData"), "config");
const profileConfigPath = path.join(
    app.getPath("documents"),
    "MS Rich Presence",
    "config"
);
const iconPath = path.join(__dirname, "icon.png");
const trayIconPath = path.join(
    __dirname,
    `${process.platform !== "darwin" ? "icon" : "mactray"}.png`
);
var win;
var tray = null;
var trayNotificationDisplayed = false;
const status = {
    started: false,
    stopped: true,
    visible: true,
    silent: false,
    developerMode: false,
    trayOnClose: true,
    launchOnStartup: false,
};
const ConfigKeys = [
    "silent",
    "developerMode",
    "trayOnClose",
    "launchOnStartup",
];

async function createAppConfigDir() {
    try {
        await fs.access(path.join(appConfigPath, "config.json"));
    } catch {
        await fs.mkdir(appConfigPath, {
            recursive: true,
        });
        await writeAppConfig(
            {
                silent: false,
                developerMode: false,
                trayOnClose: true,
                launchOnStartup: false,
            },
            true
        );
    }
}

function loadAppConfig() {
    getAppConfig().then((data) => {
        for (let [key, val] of Object.entries(data)) {
            status[key] = val;
        }
        status.launchOnStartup = data.launchOnStartup;
        setLaunchOnStartup().catch((err) => {});
    });
}

async function getAppConfig(key, defaultValue) {
    await createAppConfigDir();
    let contents = await fs.readFile(path.join(appConfigPath, "config.json"), {
        encoding: "utf-8",
    });
    let data = JSON.parse(contents);
    if (key === undefined || key === null) {
        let requireUpdate = false;
        for (let key of ConfigKeys) {
            if (data[key] === undefined) {
                data[key] = status[key];
                requireUpdate = true;
            }
        }
        if (requireUpdate) {
            await writeAppConfig(data);
        }
        return data;
    }
    return data[key] === undefined ? defaultValue : data[key];
}

async function writeAppConfig(data, dirCreated) {
    if (!dirCreated) {
        await createAppConfigDir();
    }
    await fs.writeFile(
        path.join(appConfigPath, "config.json"),
        JSON.stringify(data, null, 4),
        {
            flag: "w",
        }
    );
}

async function createProfileConfigDir() {
    try {
        await fs.access(profileConfigPath);
    } catch {
        await fs.mkdir(profileConfigPath, {
            recursive: true,
        });
    }
}

async function createNotification(body, override = false, click) {
    if (override || !status.silent) {
        let notif = new Notification({
            title: "MS Discord Rich Presence",
            body: body,
            icon: iconPath,
            timeoutType: "default",
        });
        if (click !== undefined) {
            notif.on("click", click);
        }
        notif.show();
    }
}

async function setLaunchOnStartup() {
    app.setLoginItemSettings({
        openAtLogin: status.launchOnStartup,
        enabled: true,
        name: "MS Discord Rich Presence",
    });
}

function updateContextMenu() {
    if (tray) {
        tray.setContextMenu(
            new Menu.buildFromTemplate([
                {
                    label: "MS Discord Rich Presence",
                    type: "normal",
                    enabled: false,
                },
                {
                    type: "separator",
                },
                {
                    label: "Start Rich Presence",
                    type: "normal",
                    click: () => {
                        win.webContents.send("tray-start-rpc");
                    },
                    enabled: !status.started,
                },
                {
                    label: "Stop Rich Presence",
                    type: "normal",
                    click: () => {
                        win.webContents.send("tray-stop-rpc");
                    },
                    enabled: !status.stopped,
                },
                {
                    type: "separator",
                },
                {
                    label: "Restore",
                    type: "normal",
                    click: () => {
                        win.show();
                        status.visible = true;
                        updateContextMenu();
                    },
                    enabled: !status.visible,
                },
                {
                    label: "Quit",
                    type: "normal",
                    click: () => {
                        win.webContents.send("tray-close-app");
                        win.close();
                    },
                },
                {
                    type: "separator",
                },
                new MenuItem({
                    label: "Show Notifications",
                    type: "checkbox",
                    click: async (menuItem) => {
                        status.silent = !status.silent;
                        config = await getAppConfig();
                        config.silent = status.silent;
                        await writeAppConfig(config);
                        updateContextMenu();
                    },
                    checked: !status.silent,
                }),
                new MenuItem({
                    label: "Close to Tray",
                    type: "checkbox",
                    click: async (menuItem) => {
                        config = await getAppConfig();
                        config.trayOnClose = !config.trayOnClose;
                        status.trayOnClose = menuItem.checked =
                            config.trayOnClose;
                        await writeAppConfig(config);
                        updateContextMenu();
                    },
                    checked: status.trayOnClose,
                }),
                new MenuItem({
                    label: "Open on Startup",
                    type: "checkbox",
                    click: async (menuItem) => {
                        config = await getAppConfig();
                        config.launchOnStartup = !config.launchOnStartup;
                        status.launchOnStartup = menuItem.checked =
                            config.launchOnStartup;
                        await writeAppConfig(config);
                        await setLaunchOnStartup();
                        updateContextMenu();
                    },
                    checked: status.launchOnStartup,
                }),
                new MenuItem({
                    label: "DevTools",
                    type: "checkbox",
                    click: (menuItem) => {
                        if (win.webContents.isDevToolsOpened()) {
                            win.webContents.closeDevTools();
                        } else {
                            win.webContents.openDevTools({
                                mode: "detach",
                            });
                        }
                        menuItem.checked = win.webContents.isDevToolsOpened();
                    },
                    visible: status.developerMode,
                    checked:
                        status.developerMode &&
                        win.webContents.isDevToolsOpened(),
                }),
            ])
        );
    }
}

function createTray() {
    if (tray === null) {
        tray = new Tray(trayIconPath);
        tray.on("click", (event) => {
            win.show();
            win.focus();
            status.visible = true;
            updateContextMenu();
        });
        tray.on("right-click", (event) => {
            tray.popUpContextMenu();
        });
        tray.setToolTip("MS Discord Rich Presence");
        updateContextMenu();
    }
}

function getVersionValue(string) {
    let splitVersion = string.split(".");
    let sum = 0;
    for (let index in splitVersion) {
        sum +=
            parseInt(splitVersion[index]) *
            10 ** (splitVersion.length - index - 1);
    }
    return sum;
}

async function checkForUpdates() {
    let info = await (
        await fetch(
            "https://gitlab.com/marwynnsomridhivej/msrp/-/raw/master/version.json"
        )
    ).json();
    if (getVersionValue(app.getVersion()) < getVersionValue(info.version)) {
        createNotification(
            `A newer version (v${info.version}) has been released. Click here for more information.`,
            true,
            (event) => {
                shell.openExternal(info.url + info.version);
            }
        );
    }
}

// APP SETUP
if (process.platform === "win32") {
    app.setAppUserModelId("MS Discord Rich Presence");
}

app.whenReady().then(async () => {
    await createAppConfigDir();
    await createProfileConfigDir();
    loadAppConfig();
    win = new BrowserWindow({
        width: 500,
        height: screen.getPrimaryDisplay().bounds.height > 768 ? 800 : 700,
        title: "MS Discord Rich Presence",
        resizable: false,
        fullscreenable: false,
        icon: path.resolve(__dirname, "icon.png"),
        frame: false,
        webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true,
            contextIsolation: false,
        },
    });
    win.once("ready-to-show", async () => {
        win.show();
        createTray();
        await checkForUpdates();
    });
    win.webContents.on("devtools-closed", updateContextMenu);
    win.webContents.on("devtools-opened", updateContextMenu);
    win.setBackgroundColor("#23272a");
    await win.loadFile("index.html");
});

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("second-instance", (event, commandLine, workingDirectory) => {
    if (win) {
        if (!win.isFocused()) {
            status.visible = true;
            win.show();
            updateContextMenu();
        }
        win.focus();
    }
});

//IPC SETUP

ipcMain.on("minimise-app", (event, args) => {
    let window = BrowserWindow.getFocusedWindow();
    window.minimize();
});

ipcMain.on("close-app", async (event) => {
    if (
        process.platform === "win32" &&
        (await getAppConfig("trayOnClose", true)) === true
    ) {
        win.hide();
        status.visible = false;
        updateContextMenu();
        if (!trayNotificationDisplayed) {
            createNotification(
                'I\'m now running in the background. Right click the tray icon and select "Quit" to quit',
                true
            );
            trayNotificationDisplayed = true;
        }
    } else {
        updateContextMenu();
        app.quit();
    }
});

ipcMain.on("get-config", async (event, key, defaultValue) => {
    return await getAppConfig(key, defaultValue);
});

ipcMain.on("set-config", async (event, data) => {
    return await writeAppConfig(data);
});

ipcMain.on("load-profile", async (event) => {
    await createProfileConfigDir();
    let res = await dialog.showOpenDialog(BrowserWindow.getFocusedWindow(), {
        title: "Load Profile",
        defaultPath: profileConfigPath,
        buttonLabel: "Load Profile",
        filters: [
            {
                name: "MSRP Profiles",
                extensions: ["json"],
            },
        ],
        properties: ["openFile", "dontAddToRecent"],
    });
    if (res.canceled) {
        event.reply("load-profile-result", {});
    } else {
        let contents = await fs.readFile(res.filePaths[0], {
            encoding: "utf-8",
        });
        event.reply("load-profile-result", JSON.parse(contents));
    }
});

ipcMain.on("save-profile", async (event, data) => {
    await createProfileConfigDir();
    let res = await dialog.showSaveDialog(BrowserWindow.getFocusedWindow(), {
        title: "Save Profile",
        defaultPath: profileConfigPath,
        buttonLabel: "Save Profile",
        filters: [
            {
                name: "MSRP Profiles",
                extensions: ["json"],
            },
        ],
        nameFieldLabel: "Enter the name you'd like name your profile",
    });
    if (!res.canceled) {
        await fs.writeFile(res.filePath, JSON.stringify(data, null, 4));
    }
});

ipcMain.on("rpc-started", async (event) => {
    await createNotification("Your custom rich presence has been started!");
    status.started = true;
    status.stopped = false;
    updateContextMenu();
});

ipcMain.on("rpc-stopped", async (event) => {
    await createNotification("Your custom rich presence has been stopped!");
    status.started = false;
    status.stopped = true;
    updateContextMenu();
});
