export function getattr(object, attr) {
    for (let [key, val] of Object.entries(object)) {
        if (key === attr) {
            return val;
        }
    }
    return undefined;
}
