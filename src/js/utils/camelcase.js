import { title } from "./strtitle";

export function camelCase(string, sep) {
    return string
        .split(sep)
        .map((substr, index) => {
            if (index === 0) {
                return substr;
            }
            return title(substr);
        })
        .join("");
}
