export function title(string) {
    let ret = [];
    for (let substring of string.split(" ")) {
        ret.push(substring[0].toUpperCase() + substring.substring(1));
    }
    return ret.join(" ");
}
