export function emptyUndefined(string) {
    return string !== "" ? string : undefined;
}
