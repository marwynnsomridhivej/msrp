import React from "react";

export default class Param extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            onChange: props.onChange,
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.data !== this.props.data) {
            this.setState({ data: this.props.data });
        }
    }

    render() {
        return (
            <div class="param">
                <label {...this.state.data.label}>
                    {this.state.data.label.text}
                </label>
                <input
                    {...this.state.data.input}
                    onChange={this.state.onChange}
                />
                {(() => {
                    if (this.state.data.input.type === "checkbox") {
                        return (
                            <span
                                id="checkbox"
                                onClick={this.state.onChange}
                                ischecked={`${this.state.data.input.checked}`}
                            ></span>
                        );
                    }
                })()}
            </div>
        );
    }
}
