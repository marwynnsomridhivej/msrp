import React from "react";

export default class Footer extends React.Component {
    render() {
        return (
            <div id="footer">
                <p>v1.1.2 - by Marwynn Somridhivej</p>
            </div>
        );
    }
}
