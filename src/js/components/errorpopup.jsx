import React from "react";

export default class ErrorPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errors: props.errors,
            active: props.errors.length > 0 ? true : false,
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.errors !== this.props.errors) {
            this.setState({
                errors: this.props.errors,
                active: this.props.errors.length > 0 ? true : false,
            });
        }
    }

    render() {
        return (
            <div
                id="error-container"
                active={`${this.state.active}`}
                onClick={(event) => {
                    if (event.target.id === "error-container") {
                        this.setState({ active: !this.state.active });
                    }
                }}
            >
                <div id="error-modal">
                    <div id="red-x-container">
                        <img id="red-x" src="./static/redx.svg" />
                    </div>
                    <img
                        id="close"
                        src="./static/closemodal.svg"
                        onClick={() => {
                            this.setState({ active: !this.state.active });
                        }}
                    />
                    <div id="error-messages">
                        {this.state.errors.map((error) => (
                            <p>{error}</p>
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}
