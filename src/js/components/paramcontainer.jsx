import React from "react";

export default class ParamContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            children: props.children,
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.children !== this.props.children) {
            this.setState({ children: this.props.children });
        }
    }

    render() {
        return <div class="param-container">{this.state.children}</div>;
    }
}
