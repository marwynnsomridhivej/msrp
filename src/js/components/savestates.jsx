import React from "react";
const { ipcRenderer } = require("electron");

export default class SaveStates extends React.Component {
    constructor(props) {
        super(props);
        this.saveProfile = this.saveProfile.bind(this);
        this.loadProfile = this.loadProfile.bind(this);
        this.state = {
            getMainState: props.getMainState,
            isValidProfile: props.isValidProfile,
        };
    }

    saveProfile(event) {
        event.preventDefault();
        ipcRenderer.send("save-profile", this.state.getMainState());
    }

    loadProfile(event) {
        event.preventDefault();
        ipcRenderer.send("load-profile");
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.getMainState !== this.props.getMainState) {
            this.setState({ getMainState: this.props.getMainState });
        }
        if (prevProps.isValidProfile !== this.props.isValidProfile) {
            this.setState({ isValidProfile: this.props.isValidProfile });
        }
    }

    render() {
        return (
            <div id="savestates-container">
                <button onClick={this.loadProfile}>Load Profile</button>
                <button
                    onClick={this.saveProfile}
                    disabled={!this.state.isValidProfile}
                >
                    Save Profile
                </button>
            </div>
        );
    }
}
