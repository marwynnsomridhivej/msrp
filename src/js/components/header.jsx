import React from "react";
import { getattr } from "../utils/pygetattr";
import { title } from "../utils/strtitle";
const { ipcRenderer } = require("electron");
const buttons = ["min", "close"];

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.closeHandler = props.close;
        this.setMin = this.setMin.bind(this);
        this.setClose = this.setClose.bind(this);
    }

    setMin(event) {
        event.preventDefault();
        ipcRenderer.send("minimise-app");
    }

    setClose(event) {
        event.preventDefault();
        ipcRenderer.send("close-app");
    }

    genButtons() {
        return buttons.map((button) => (
            <div
                id="header-button-wrapper"
                onClick={getattr(this, `set${title(button)}`)}
            >
                <img id={button} src={`./static/${button}.svg`} />
            </div>
        ));
    }

    render() {
        return (
            <div id="header">
                <h1>MS Discord Rich Presence</h1>
                <div id="header-buttons">{this.genButtons()}</div>
            </div>
        );
    }
}
