import React from "react";
import DiscordRPC from "discord-rpc";
import Preloader from "./preloader.jsx";
import Header from "./header.jsx";
import ParamContainer from "./paramcontainer.jsx";
import Param from "./param.jsx";
import SaveStates from "./savestates.jsx";
import ErrorPopup from "./errorpopup.jsx";
import Footer from "./footer.jsx";
import { getattr } from "../utils/pygetattr.js";
import { emptyUndefined } from "../utils/undef.js";

const { ipcRenderer } = require("electron");
var rpc = null;

export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.startRPC = this.startRPC.bind(this);
        this.stopRPC = this.stopRPC.bind(this);
        this.closeRPC = this.closeRPC.bind(this);
        this.getMainState = this.getMainState.bind(this);
        this.state = {
            usedClientID: "",
            clientID: "",
            details: "",
            state: "",
            showStartTime: false,
            startTimestamp: "",
            largeImageKey: "",
            largeImageText: "",
            smallImageKey: "",
            smallImageText: "",
            button1Label: "",
            button2Label: "",
            button1URL: "",
            button2URL: "",
            errors: [],
            isValid: false,
        };
    }

    componentDidMount() {
        ipcRenderer.on("load-profile-result", (event, data) => {
            let newState = {};
            for (let [key, val] of Object.entries(data)) {
                newState[key] = val;
            }
            this.setState(newState, () => {
                this.validateForm(true);
            });
        });

        ipcRenderer.on("tray-start-rpc", async (event) => {
            await this.startRPC();
        });

        ipcRenderer.on("tray-stop-rpc", async (event) => {
            await this.stopRPC();
        });

        ipcRenderer.on("tray-close-app", async (event) => {
            await this.closeRPC();
        });
    }

    validateForm(silent) {
        let errors = [];
        if (Number.isNaN(parseInt(this.state.clientID))) {
            errors.push("Client ID must be a valid integer");
        }
        if (this.state.clientID < 0) {
            errors.push("Client ID cannot be a negative number");
        }
        if (this.state.clientID.length !== 18) {
            errors.push("Client ID must be 18 characters long");
        }
        if (this.state.details !== "" && this.state.details.length < 2) {
            errors.push(
                "If specified, details must be at least two characters"
            );
        }
        if (this.state.state !== "" && this.state.state.length < 2) {
            errors.push("If specified, state must be at least two characters");
        }
        for (let index of [1, 2]) {
            if (
                !getattr(this.state, `button${index}Label`) &&
                getattr(this.state, `button${index}URL`)
            ) {
                errors.push(`Button ${index} must have a label`);
            }
            if (
                getattr(this.state, `button${index}Label`) &&
                !getattr(this.state, `button${index}URL`)
            ) {
                errors.push(`Button ${index} must have a URL`);
            }
        }

        if (errors.length > 0 && silent !== true) {
            this.setState({ errors: errors }, () =>
                console.error(
                    "[VALIDATE FORM]: Invalid parameters\n",
                    errors.join("\n")
                )
            );
        }

        if (this.state.isValid !== (errors.length === 0)) {
            this.setState({ isValid: errors.length === 0 });
        }
        return errors.length === 0;
    }

    async initRPC() {
        if (this.state.usedClientID !== this.state.clientID) {
            if (rpc) {
                await rpc
                    .destroy()
                    .catch((err) =>
                        console.error(
                            "[INIT RPC - DESTROY EXISTING RPC]: ",
                            err
                        )
                    );
            }
            rpc = new DiscordRPC.Client({ transport: "ipc" });
            DiscordRPC.register(this.state.clientID);
            await rpc
                .login({
                    clientId: this.state.clientID,
                })
                .then(() => {
                    this.setState({ usedClientID: this.state.clientID });
                    console.log("LOGGED IN");
                })
                .catch(async (err) => {
                    console.error("[INIT RPC]: ", err);
                    await rpc
                        .destroy()
                        .catch((err) =>
                            console.error(
                                "[INIT RPC - LOGIN - DESTROY RPC]: ",
                                err
                            )
                        );
                });
        }
    }

    async startRPC(event) {
        if (event) {
            event.preventDefault();
        }
        if (this.validateForm()) {
            await this.initRPC();
            rpc.setActivity({
                details: emptyUndefined(this.state.details),
                state: emptyUndefined(this.state.state),
                startTimestamp: (() => {
                    if (this.state.showStartTime) {
                        return this.state.startTimestamp !== ""
                            ? Date.parse(this.state.startTimestamp)
                            : Date.now().valueOf();
                    }
                    return undefined;
                })(),
                largeImageKey: emptyUndefined(this.state.largeImageKey),
                largeImageText: emptyUndefined(this.state.largeImageText),
                smallImageKey: emptyUndefined(this.state.smallImageKey),
                smallImageText: emptyUndefined(this.state.smallImageText),
                buttons: (() => {
                    let buttons = [1, 2]
                        .map((index) => {
                            return {
                                label: getattr(
                                    this.state,
                                    `button${index}Label`
                                ),
                                url: getattr(this.state, `button${index}URL`),
                            };
                        })
                        .filter((button) => button.label && button.url);
                    return buttons.length > 0 ? buttons : undefined;
                })(),
                instance: false,
            })
                .then(() => {
                    ipcRenderer.send("rpc-started");
                })
                .catch((err) => {
                    console.error("[START RPC]: ", err);
                    rpc = null;
                });
        }
    }

    async stopRPC(event) {
        if (event) {
            event.preventDefault();
        }
        if (rpc) {
            await rpc
                .clearActivity()
                .then(() => {
                    ipcRenderer.send("rpc-stopped");
                })
                .catch((err) => console.error("[STOP RPC]: ", err));
        }
    }

    async closeRPC(event) {
        if (event !== undefined) {
            event.preventDefault();
        }
        await rpc.destroy().catch((err) => console.error("[CLOSE RPC]: ", err));
    }

    genClientIDChildren() {
        return (
            <Param
                data={{
                    label: {
                        for: "clientID",
                        text: "Client ID",
                    },
                    input: {
                        id: "clientID",
                        type: "number",
                        placeholder: "Your application's client ID",
                        value: this.state.clientID,
                    },
                }}
                onChange={(event) => {
                    this.setState({ clientID: event.target.value });
                }}
            />
        );
    }

    genStateDetailsChildren() {
        return [
            <Param
                data={{
                    label: {
                        for: "details",
                        text: "Details",
                    },
                    input: {
                        id: "details",
                        type: "text",
                        placeholder: "Appears under app name",
                        value: this.state.details,
                    },
                }}
                onChange={(event) => {
                    this.setState({ details: event.target.value });
                }}
            />,
            <Param
                data={{
                    label: {
                        for: "state",
                        text: "State",
                    },
                    input: {
                        id: "state",
                        type: "text",
                        placeholder: "Appears under details text",
                        value: this.state.state,
                    },
                }}
                onChange={(event) => {
                    this.setState({ state: event.target.value });
                }}
            />,
        ];
    }

    genTimeChildren() {
        return [
            <Param
                data={{
                    label: {
                        for: "showStartTime",
                        text: "Show Start Time",
                    },
                    input: {
                        id: "showStartTime",
                        type: "checkbox",
                        checked: this.state.showStartTime,
                    },
                }}
                onChange={(event) => {
                    this.setState({ showStartTime: !this.state.showStartTime });
                }}
            />,
            <Param
                data={{
                    label: {
                        for: "startTimestamp",
                        text: "Start Time",
                    },
                    input: {
                        id: "startTimestamp",
                        type: "datetime-local",
                        value: this.state.startTimestamp,
                        disabled: !this.state.showStartTime,
                    },
                }}
                onChange={(event) => {
                    this.setState({
                        startTimestamp: event.target.value,
                    });
                }}
            />,
        ];
    }

    genImgKeyChildren() {
        return [
            <Param
                data={{
                    label: {
                        for: "largeImageKey",
                        text: "Large Image Key",
                    },
                    input: {
                        id: "largeImageKey",
                        type: "text",
                        placeholder: "Key for large image",
                        value: this.state.largeImageKey,
                    },
                }}
                onChange={(event) => {
                    this.setState({ largeImageKey: event.target.value });
                }}
            />,
            <Param
                data={{
                    label: {
                        for: "smallImageKey",
                        text: "Small Image Key",
                    },
                    input: {
                        id: "smallImageKey",
                        type: "text",
                        placeholder: "Key for small image",
                        value: this.state.smallImageKey,
                    },
                }}
                onChange={(event) => {
                    this.setState({ smallImageKey: event.target.value });
                }}
            />,
        ];
    }

    genImgTextChildren() {
        return [
            <Param
                data={{
                    label: {
                        for: "largeImageText",
                        text: "Large Image Text",
                    },
                    input: {
                        id: "largeImageText",
                        type: "text",
                        placeholder: "Text displayed on hover",
                        value: this.state.largeImageText,
                    },
                }}
                onChange={(event) => {
                    this.setState({ largeImageText: event.target.value });
                }}
            />,
            <Param
                data={{
                    label: {
                        for: "smallImageText",
                        text: "Small Image Text",
                    },
                    input: {
                        id: "smallImageText",
                        type: "text",
                        placeholder: "Text displayed on hover",
                        value: this.state.smallImageText,
                    },
                }}
                onChange={(event) => {
                    this.setState({ smallImageText: event.target.value });
                }}
            />,
        ];
    }

    genButtonLabelChildren() {
        return [
            <Param
                data={{
                    label: {
                        for: "button1Label",
                        text: "Button 1 Label",
                    },
                    input: {
                        id: "button1Label",
                        type: "text",
                        placeholder: "Label on first button",
                        value: this.state.button1Label,
                    },
                }}
                onChange={(event) => {
                    this.setState({ button1Label: event.target.value });
                }}
            />,
            <Param
                data={{
                    label: {
                        for: "button2Label",
                        text: "Button 2 Label",
                    },
                    input: {
                        id: "button2Label",
                        type: "text",
                        placeholder: "Label on second button",
                        value: this.state.button2Label,
                    },
                }}
                onChange={(event) => {
                    this.setState({ button2Label: event.target.value });
                }}
            />,
        ];
    }

    genButtonURLChildren() {
        return [
            <Param
                data={{
                    label: {
                        for: "button1URL",
                        text: "Button 1 URL",
                    },
                    input: {
                        id: "button1URL",
                        type: "url",
                        placeholder: "URL to go to on click",
                        value: this.state.button1URL,
                    },
                }}
                onChange={(event) => {
                    this.setState({ button1URL: event.target.value });
                }}
            />,
            <Param
                data={{
                    label: {
                        for: "button2URL",
                        text: "Button 2 URL",
                    },
                    input: {
                        id: "button2URL",
                        type: "url",
                        placeholder: "URL to go to on click",
                        value: this.state.button2URL,
                    },
                }}
                onChange={(event) => {
                    this.setState({ button2URL: event.target.value });
                }}
            />,
        ];
    }

    getMainState() {
        return {
            clientID: this.state.clientID,
            details: this.state.details,
            state: this.state.state,
            showStartTime: this.state.showStartTime,
            startTimestamp: this.state.startTimestamp,
            largeImageKey: this.state.largeImageKey,
            largeImageText: this.state.largeImageText,
            smallImageKey: this.state.smallImageKey,
            smallImageText: this.state.smallImageText,
            button1Label: this.state.button1Label,
            button2Label: this.state.button2Label,
            button1URL: this.state.button1URL,
            button2URL: this.state.button2URL,
        };
    }

    render() {
        return (
            <>
                <Preloader />
                <Header close={this.closeRPC} />
                <section id="main">
                    <form id="rich-presence-form">
                        <ParamContainer children={this.genClientIDChildren()} />
                        <ParamContainer
                            children={this.genStateDetailsChildren()}
                        />
                        <ParamContainer children={this.genTimeChildren()} />
                        <ParamContainer children={this.genImgKeyChildren()} />
                        <ParamContainer children={this.genImgTextChildren()} />
                        <ParamContainer
                            children={this.genButtonLabelChildren()}
                        />
                        <ParamContainer
                            children={this.genButtonURLChildren()}
                        />
                    </form>
                    <div id="rich-presence-button-container">
                        <button id="rp-stop" onClick={this.stopRPC}>
                            Stop
                        </button>
                        <button id="rp-start" onClick={this.startRPC}>
                            Start
                        </button>
                    </div>
                    <SaveStates
                        getMainState={this.getMainState}
                        isValidProfile={this.validateForm(true)}
                    />
                    <ErrorPopup errors={this.state.errors} />
                </section>
                <Footer />
            </>
        );
    }
}
