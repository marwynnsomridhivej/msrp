import React from "react";

export default class Preloader extends React.Component {
    constructor(props) {
        super(props);
        this.state = { stage: "enter" };
    }

    componentDidMount() {
        window.addEventListener("load", (event) => {
            this.setState({ stage: "exit" });
        });
    }

    render() {
        return (
            <div id="preloader" stage={this.state.stage}>
                <img id="mb" src="./static/mb-512.jpg" />
                <img id="cog" src="./static/cog.svg" />
            </div>
        );
    }
}
