import React from "react";
import ReactDOM from "react-dom";
import Main from "./components/main.jsx";

window.version = "1.1.2";

class App extends React.Component {
    render() {
        return <Main />;
    }
}

ReactDOM.render(<App />, document.querySelector("div#react-root"));
