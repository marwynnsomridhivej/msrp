const gulp = require("gulp");
const scss = require("gulp-sass");
const webpack = require("webpack-stream");
const config = require("./webpack.config.js");

gulp.task("css", () => {
    return gulp
        .src([`./src/scss/*.scss`])
        .pipe(scss({ outputStyle: "compressed" }))
        .pipe(gulp.dest("./dist/css"));
});

gulp.task("js", () => {
    return gulp
        .src(["./src/js/index.js"])
        .pipe(webpack(config))
        .pipe(gulp.dest("./dist/js"));
});

gulp.task("img", () => {
    return gulp.src(["./src/img/**/*.*"]).pipe(gulp.dest("./dist/static"));
});

gulp.task("move", () => {
    return gulp
        .src(["./src/index.html", "./src/main.js"])
        .pipe(gulp.dest("./dist/"));
});

gulp.task("default", gulp.series("css", "img", "move", "js"));
